const bluebird        = require('bluebird');
const mongoose        = require('mongoose');
const bcrypt          = bluebird.promisifyAll(require('bcrypt'));

const statusCodes     = require('./config/statusCodes');
const User            = require('./model/user/user-schema');
const EvaNAuthHelpers = require('./helpers');
const config          = require('./config/config');

let EvaNAuth = {

  init: function(router) {
    if (mongoose.connections[0].name === null){
      mongoose.Promise = bluebird;
      mongoose.connect(config.mongo.url);
      console.log('Connected to', mongoose.connections[0].name, 'database');
    }
    if (router){
      const session  = require('./model/session/session-router');
      const register = require('./model/register/register-router');
      const user     = require('./model/user/user-router');

      router.use('/session', session);
      router.use('/register', register);
      router.use('/user', user);
    }
   },

  Protect: function(req, res, next) {
    let email     = req.headers['z-user'];
    let token     = req.headers['z-token'];
    let query     = {email: email};

    User.findOne(query, function(err, user){
      // to be deleted after testing
      if (user){
        console.log('token should be ', EvaNAuthHelpers.GenerateHashForRequest(req, user.token));
      }
      // -->
      if (err) {
        res.status(statusCodes.Error.code).send(statusCodes.Error.message);
      }
      else if (EvaNAuthHelpers.UserIsValid(user) && EvaNAuthHelpers.TokenIsValid(user)) {
        if (token === EvaNAuthHelpers.GenerateHashForRequest(req, user.token)) {
          next();
        }
        else {
          res.status(statusCodes.Authorization.code).send(statusCodes.Authorization.message);
        }
      }
      else {
        res.status(statusCodes.Authorization.code).send(statusCodes.Authorization.message);
      }
    });
   },

  UpdateTokenForUser: function(user) {
    let query   = {email: user.email};
    let options = {new: true};

    return this.CreateTokenByEmail(user.email)
    .then(function(update){

      return User.findOneAndUpdate(query, update, options);
    })
    .then(function(user){

      return user.token;
    });
   },

  CreateTokenByEmail: function(email) {
     let update = {};
     let random =  Math.random().toString(36).substr(2);
     let unHashedToken = random + email;

    return bcrypt.genSaltAsync(10)
    .then(function(salt) {

      return bcrypt.hashAsync(unHashedToken, salt);
    })
    .then(function(hash){
      update.token = hash;
      update.tokenCreated =  Date.now() /1000 | 0;

      return update;
    });
  },

  GetCurrentUser: function(req) {
    let email     = req.headers['z-user'];
    let query     = {email: email};

     return User.findOne(query, function(err, user){
      if (err) {
        //Promise.reject(err);
      }
      if (user){
        return user
      }
      else{
        //Promise.reject('no user');
      }
    });
  }
}

module.exports = EvaNAuth;