const controller = require('./user-controller');
const Router = require('express').Router;
const router = new Router();
const EvaNAuth   = require('../../evanauth');

router.use(EvaNAuth.Protect);

router.route('/')
  .get((...args) => controller.find(...args))
  .post((...args) => controller.create(...args));

router.route('/:id')
  .put((...args) => controller.update(...args))
  .get((...args) => controller.findById(...args))
  .delete((...args) => controller.remove(...args));

module.exports = router;
