const User       = require('../user/user-schema');
const Router     = require('express').Router;
const router     = new Router();
const EvaNAuth   = require('../../evanauth.js');

class SessionController{

  login(req, res, next) {
    User.findOne({
    email: req.body.email
  }, function(err, user) {
    if (err) throw err;
 
    if (!user) {
      res.send({success: false, msg: 'Authentication failed. User not found.'});
    } else {
      user.comparePassword(req.body.password, function (err, isMatch) {
        if (isMatch && !err) {
          EvaNAuth.UpdateTokenForUser(user).then(function(token) {
            res.json({success: true, token: token});
          });
        } else {
          res.send({success: false, msg: 'Authentication failed. Wrong password.'});
        }
      });
    }
  });
  }

  logout(req, res, next) {
    let query   = {email: req.headers['z-user']};
    let update  = { token: '0' };

    User.findOneAndUpdate(query, update)
    .then(function(user){

      if (!user) {
        res.send({success: false, msg: 'User not found.'});
      } 
      else {
        res.send({success: true, msg: 'User logged out.'});
      }


    }); 
  }

}

module.exports = new SessionController();
