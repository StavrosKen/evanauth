const controller = require('./session-controller');
const Router     = require('express').Router;
const router     = new Router();
const EvaNAuth   = require('../../evanauth');

router.route('/')
  .post((...args) => controller.login(...args));

router.use(EvaNAuth.Protect);

router.route('/')
  .delete((...args) => controller.logout(...args));

module.exports = router;
