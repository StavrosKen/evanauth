const controller = require('./register-controller');
const Router     = require('express').Router;
const router     = new Router();
const EvaNAuth   = require('../../evanauth');

router.route('/')
  .post((...args) => controller.register(...args));

router.use(EvaNAuth.Protect);

router.route('/')
  .delete((...args) => controller.unregister(...args));

module.exports = router;
