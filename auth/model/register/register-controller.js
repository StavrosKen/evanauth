const Controller  = require('../../lib/controller');
const user        = require('../user/user-schema');
const statusCodes = require('../../config/statusCodes');

class RegisterController extends Controller{

  register(req, res, next) {
  if (!req.body.email || !req.body.password) {
    res.json({success: false, msg: 'Please pass email and password.'});
  } else {
    var newUser = new user({
      email: req.body.email,
      password: req.body.password
    });
    // save the user
    newUser.save(function(err) {
      if (err) {
        console.log(err);
        return res.json({success: false, msg: 'Email already exists.'});
      }
      res.json({success: true, msg: 'Successfully created new user.'});
    });
  }
  }

  unregister(req, res, next) {
    let query   = {email: req.headers['z-user']};
    let update  = {active: false};

    user.findOneAndUpdate(query, update)
    .then(function(user){

      if (!user) {
        res.send({success: false, msg: 'User not found.'});
      } 
      else {
        res.send({success: true, msg: 'User unregistered.'});
      }


    }); 
  }

}

module.exports = new  RegisterController();
