const config = {
  mongo: {
    url: process.env.MONGO_DB_URI || 'mongodb://localhost/evanauth'
  },
  sessionMaxDuration: 7 * 24 * 60 * 60
};

module.exports = config;
