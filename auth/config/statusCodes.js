const httpStatusCodes = {

  Error: {
    code: 400,
    message: 'Something went wrong.'
  },
  
  Authorization: {
    code: 403,
    message: 'Unauthorized.'
  }
};

module.exports = httpStatusCodes;
