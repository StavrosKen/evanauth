const sha1   = require('sha1');
const config = require('./config/config');

module.exports = {

  GenerateHashForRequest: function(req, token){
    let hash;
    let url = "http://" + req.get('host') + req.originalUrl;
    if (req.method === 'POST') {
      hash = (url + JSON.stringify(this.serializeRgQuery(req.body)) + token);
    } 
    else {
      hash = (url + token);
    }
    return sha1(hash);
  },

  serializeRgQuery: function(obj, prefix) {
    let str = [];
    for (let p in obj) {
        let k = prefix ? prefix + "[" + p + "]" : p,
            v = obj[p];
        str.push(typeof v === "object" ?
            encodeURIComponent(k) + "=" + encodeURIComponent(JSON.stringify(v)).replace('%3A', ':') :
            encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
    return str.join("&");
},

  UserIsValid: function(user) {
    let userIsValid = false;
    if (user && user.active) {
      userIsValid = true;
    }
    return userIsValid;
  },

  TokenIsValid: function(user) {
    let tokenIsValid = false;
    let tokenExpirationDate = user.tokenCreated + config.sessionMaxDuration;
    let timeNow = Date.now() / 1000;
    if (user.token != 0){
      if (tokenExpirationDate > timeNow) {
        tokenIsValid = true;
      }
    }
    return tokenIsValid;
  }

};