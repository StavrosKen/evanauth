/*!
 * evanauth
 * Copyright(c) 2017 Stavros Kainich
 * Copyright(c) 2017 Evalest Ltd
 * MIT Licensed
 */

'use strict';

module.exports = require('./auth/evanauth.js');
