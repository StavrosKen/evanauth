# EvaNAuth - The Evalest Node Authentication

> npm module that adds secure authorization capabilities to your node API

## Features
Out of the box, you 'll get the following endpoint capabilities :

* **Register** - *subscribes a new user to your api*
* **Unregister**  - *unsibscribes a user to your api*
* **Login** - *creates session*
* **Logout** - *destroys session*
* **User** - *CRUD operations to your users*

You will also get the following functionalities:

* **Session expiration** 
* **Endpoint Protection Middleware**

## Installation

- Install evanauth in your project: `npm install evanauth --save`
- Create a local empty MongoDB database named 'evanauth'

## Set up

- Include `evanauth` and `body-parser` in your Express.js project
 ```javascript
 const bodyParser  = require('body-parser');
 const EvaNauth = require('evanauth');
 ```
- Make you app use `body-parser`
 ```javascript
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
```
- After you define a starting point of your router, call Evanauth passing your router
 ```javascript
EvaNauth.init(router);
```
You now have all default endpoints configured and your api is ready to manipulate users.
## Endpoint Protection Usage

To protect any endpoint of your api, call the EvaNauth middleware "Protect"
 ```javascript
 router.use(EvaNauth.Protect);
 // after EvaNauth middleware, all routes are protected
```
All endpoints before the Protect middleware, will be publicly accessible from anyone.
All endpoints defined after that will be protected and accessible only by authorized users.

## How it works

When you instantiate it, EvaNauth listens for calls to specific endpoints:

Unprotected:
* /register, POST (email, password)
* /session, POST (email, password)

Protected:
* /register, DELETE
* /session, DELETE
* /user, GET
* /user, POST
* /user/:id, GET
* /user/:id, PUT
* /user/:id, DELETE

Each, responds appropriately, communicating with its associated database

You can protect any endpoint using the 'Protect' middleware of EvaNauth. This, will receive the HTTP headers send with the request and validate the user against these before proceeding with the normal execution of your endpoints.

### A high level description of the authentication method:

EvaNauth expects a 'z-user' and a 'z-token' HTTP header along with every request to a protected endpoint.
'z-user' is the current user's email exactly as he mentioned it during the registration.
'z-token' should be created by the forntend and it should be the sha1 version of the concatination of several things. 
These are:
- The full url the request is sent to
- The string version of the request body (if it is a POST request)
- The token the user was provided with during the login process (normally stored in a cookie or local storage)

When a request is made to a protected endpoint, EvaNauth will use the z-user header to find the user who makes the call.
If he exists and he is not unregistered (user.active == false), it will retrieve his most recent token.
If his token is still valid and not expired, it will create a hash-token.
The hash token is a sha1 version of the concatination of following things:
- The full url the user requested
- The string version of the request body (if it is a POST request)
- The most recen user token.

If the hash-token and the z-token match it will grant the request access to the protected endpoint.

## Extras

- EvaNauth exposes a function GetCurrentUser for your convenience to retrieve the user that made the current request.
- It also utilizes date expiration of the session. This defaults to 7 days.
- See the example project located inside the EvaNauth folder for a demonstration.

## License

[MIT license](http://opensource.org/licenses/mit-license.php)