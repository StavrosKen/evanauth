const express     = require('express');
const bodyParser  = require('body-parser');

const app         = express();
const Router      = require('express').Router;
const router      = new Router();

const EvaNauth = require('../index.js');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/', router);

EvaNauth.init(router);

//this is an unprotected route
router.route('/').get((req, res) => {
  res.json({ message: 'Welcome to EvaNauth' });
});

router.use(EvaNauth.Protect);
// all following routes are protected

router.route('/currentuser').get((req, res) => {
	EvaNauth.GetCurrentUser(req).then(function(user){
		res.json({ userObj: user });
	});	
});

app.listen(8015, () => {
  console.log(`App runs on port 8015`);
});

module.exports = app;
